package com.iatskaer.books.ejb;

import com.iatskaer.books.Book;
import com.iatskaer.books.BookDB;

import jakarta.ejb.Stateless;
import java.util.ArrayList;

@Stateless
public class SessionBean {
    public ArrayList<Book> getBooks() {
        return BookDB.select();
    }
}
