package com.iatskaer.books;

import java.sql.*;
import java.util.ArrayList;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class BookDB {
    public static ArrayList<Book> select() {
        ArrayList<Book> books = new ArrayList<>();
        try {
            Context context = new InitialContext();
            DataSource dataSource = (DataSource) context.lookup("java:/library");
            
            try (Connection conn = dataSource.getConnection()) {
                String sql = "SELECT * FROM books";
                try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                    ResultSet resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        String title = resultSet.getString("title");
                        String author = resultSet.getString("author");
                        Book book = new Book(id, title, author);
                        books.add(book);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return books;
    }

    public static int insert(Book book) {
        try {
            Context context = new InitialContext();
            DataSource dataSource = (DataSource) context.lookup("java:/library");
            
            try (Connection conn = dataSource.getConnection()) {
                String sql = "INSERT INTO books (title, author) VALUES (?, ?)";
                try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                    preparedStatement.setString(1, book.getTitle());
                    preparedStatement.setString(2, book.getAuthor());
                    return preparedStatement.executeUpdate();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static Book selectOne(int id) {
        Book book = null;
        try {
            Context context = new InitialContext();
            DataSource dataSource = (DataSource) context.lookup("java:/library");
            
            try (Connection conn = dataSource.getConnection()) {
                String sql = "SELECT * FROM books WHERE id=?";
                try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                    preparedStatement.setInt(1, id);
                    ResultSet resultSet = preparedStatement.executeQuery();
                    if (resultSet.next()) {
                        int bookId = resultSet.getInt("id");
                        String title = resultSet.getString("title");
                        String author = resultSet.getString("author");
                        book = new Book(bookId, title, author);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return book;
    }

    public static int update(Book book) {
        try {
            Context context = new InitialContext();
            DataSource dataSource = (DataSource) context.lookup("java:/library");
            
            try (Connection conn = dataSource.getConnection()) {
                String sql = "UPDATE books SET title=?, author=? WHERE id=?";
                try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                    preparedStatement.setString(1, book.getTitle());
                    preparedStatement.setString(2, book.getAuthor());
                    preparedStatement.setInt(3, book.getId());
                    return preparedStatement.executeUpdate();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static int delete(int id) {
        try {
            Context context = new InitialContext();
            DataSource dataSource = (DataSource) context.lookup("java:/library");
            
            try (Connection conn = dataSource.getConnection()) {
                String sql = "DELETE FROM books WHERE id=?";
                try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                    preparedStatement.setInt(1, id);
                    return preparedStatement.executeUpdate();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }
}
