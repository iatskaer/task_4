<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/header.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>Ошибка</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h1>Ошибка</h1>
    <p>Произошла ошибка.</p>
</div>
</body>
</html>
<%@ include file="/footer.jspf"%>
