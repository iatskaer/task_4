<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/header.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>Не найдено</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h1>Не найдено</h1>
    <p>Запрошенная книга не найдена.</p>
</div>
</body>
</html>
<%@ include file="/footer.jspf"%>
