<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/header.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>Список книг</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h1>Список книг</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Автор</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${books}" var="book">
            <tr>
                <td>${book.id}</td>
                <td>${book.title}</td>
                <td>${book.author}</td>
                <td>
                    <a href="view?id=${book.id}" class="btn btn-primary">Просмотреть</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <a href="add" class="btn btn-success">Добавить книгу</a>
</div>
</body>
</html>
<%@ include file="/footer.jspf"%>
